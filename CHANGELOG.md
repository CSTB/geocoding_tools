

# 0.1.0 gorenove release

## addok utils

* improve performance by removing addr duplicates prior to addok search. 

# 0.0.3 CITYCODE to POSTCODE

## BAN UTILS
* improve management of "la corse"

* adding alternatives sources for correspondance of postal_codes and citycode

    * communes2020 (insee)
    * laposte_hexasmal

## geocode_table

* method to impute postcode from citycode if no postcode available on source dataset
This is done because scores from BAN are much better with postcodes. 

# 0.0.2 Minor Improvements : first batch
## misc 

various debug of minor functionalities not working as intended in 0.0.1

## Geocode table 

adding rule to add cityname if only citycode provided as a city input

fix issue with multiple id_addr

## addr_utils

ignored special character list improved

managing a small pool of abreviations 

drop repeated consecutive words to improve geocoding perf ex : "2 2 cours michu tartampion tartampion" -> "2 cours michu tartampion"

# scripts_specific_bases

## DPE

adding a wide cleaning of all special chars from strings. 


# 0.0.3 ->0.04  bugfixes

# 0.0.5 optimize request with geocoding an address only once

# 0.0.6 add option of trust codes

* add an option for the user to specify they trust the citycode/postcode provided and therefore
forbids a geocoding outside the boundaries of the citycode provided. 
(replacement of the non working feature of search_csv adding citycode , postcode as argument but not working.)

# 0.0.7 add remove_stacked_address methods

* add two methods in geocode_table to remove stacked address that are probably errors. 

# 0.0.8 proper error raised when geocode failed completeley

* new error type AddokFullFailGeocodeException

# 0.0.9 tagging reject and fix null address concat

* what the title says


# 0.1.0 fixing issue on related cities

* MAJOR FIX if trusted_codes is code_insee , now the algorithm consider to be valid if it is a code_insee of a town that is associated (neighborhood for big cities etc..)

* add abv_words from DGFIP

# 0.1.0 fix bad address_concat when advanced

* what the title says

# 0.1.1

bugfixes

# 0.1.2

bugfixes

# 0.2.0 ajout de postprocessing

# 0.2.1 refactor clean addr

* homogénéisation des traitements de néttoyage d'adresses. 

# 0.2.2 fix rejected score

* ne pas rejeter lorsque le résultat n'est pas housenumber

* ajout za et zi  aux abv

# 0.2.3 fix problème de géocodage quand retour null de cityname search

* fix problème de géocodage quand retour null de cityname search

# 0.2.5 fix problème de postprocessing lorsque le résultat de géocodage est null

* le recovery score calcule nan lorsque result_label ou adress_sent sont null

# 0.3 développement de l'option run_addok_option

* possibilté de switch entre search_csv et search pour les fonctions simple et advanced.

# 0.3.1

* modification du partial ratio qui renvoi un score NULL quand les deux adresses comparées n'ont pas le même code postal. 

# 0.3.2

* switch des prints sur un logger propre et configurable

# 0.3.3

* ajout d'un test d'addok sur des adresses fonctionnelles qui fait crasher tout lancement de géocodage s'il ne réussi pas. 

# 0.4.0

* modification du setup pour une install propre d'addok taggé 

* bugfixes sur le geocodage addok natif

# 0.5.0

* remove_stacked_address_behind_best,remove_stacked_address_strict_best -> passage dans le module postprocessing

* remove_stacked_address_behind_best,remove_stacked_address_strict_best : ajout d'une option pour utiliser le max de recovery score et de result score pour le filtre. 

# 0.6.0

* passage des libs de visualisation et de geopandas en optionnel.

# 0.6.1

* fix : addok install

# 0.7.0

* fix addok csv avec nouvelle api d'addok

# 0.7.1

* fix url addok custom https://github.com/AntoineBreitwillerCSTB/addok/archive/refs/heads/1.0.2-fix.zip

# 0.8.0

* ajout d'une fonctionalité pour corriger les lettres isolées dans les adresses (ex. 1 A Lotissement le Clos des Moissons 01340 Jayat ->  1a Lotissement le Clos des Moissons 01340 Jayat)

# 0.9.0

* compatibilité pandas > 2.0 changement des méthodes .append de pandas en pd.concat

# 0.10.0

## dictionnaries

*  ' ter ': ' terre ', # TER est une suffixe d'adresse à ne surtout pas transformer en TERRE ! 
*  amélioration du traitement des lettres isolées en incluant la possiblité de remplacer b -> bis , t -> ter

# 0.11.0

* ajout d'un bonus de scoring pour prioriser les communes sources dans le cas où deux adresses sont identiques dans deux communes partageant le même code postal.

Ex:  1 Rue du General Leclerc 67440 Sommerau, 1 Rue du General Leclerc 67440 Marmoutier seront testés si 1 Rue du General Leclerc 67440 Marmoutier est envoyé au géocodeur. Les adresses provenant d'un autre code postal sont un peu pénalisé pour s'assurer que la bonne adresse sort bien à la fin.

* ajout en cas de commmune regroupé le nom de l'ancienne commune entre parenthèses comme le fait la BAN 

Ex:  1 Rue du General Leclerc 67440 Sommerau -> echec car dans la BAN l'adresse est 1 Rue du General Leclerc (Singrist) 67440 Sommerau -> mauvais score. Avec cette modification l'ajout de la commune ancienne entre parenthèse permet de matcher correctement avec la BAN

# 0.12.0

*  amélioration du traitement des bis et ter non collés au numéro de rue

# 0.12.3

* amélioration de l'algorithme de prise en compte des anciennes communes en utilisant la BAN directement

# 0.12.4

* fix a bug with trusted_codes = []

# 0.12.5

* fix : gestion des départements outremer renseignés avec des codes départements à deux chiffres

# 0.12.6

* fix : compatibilité pandas > 2.0 changement des méthodes .append de pandas en pd.concat

# 0.13.0

* feat : ajout d'un système de cache qui permet de ne requêter les communes par département qu'une seule fois et après d'utiliser un fichier gardé en cache. 

