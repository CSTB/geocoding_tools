from setuptools import find_packages, setup

import sys

install_requires = ['fuzzywuzzy',

                    "pandas>=1.0.3",
                    ]
if 'win' not in sys.platform :
    install_requires.append('python-Levenshtein')

setup(
    name='geocoding_tools',
    version='0.13.0',
    url='https://gitlab.com/gorenove/geocoding_tools',
    packages=find_packages(),
    install_requires=install_requires,
    package_data = {'': ['*.txt','*.csv','*.zip'],
                                        'geocoding_tools':['tests/sample_data/*.csv']
                                     },
    extras_require={
        'test': ['pytest'],
        "geo": ['geopandas>=0.6.1',    'pyproj',
                    'shapely'],
        "viz": ["folium",
                    'contextily'],
        'native_addok': ['addok @  https://github.com/AntoineBreitwillerCSTB/addok/archive/refs/heads/1.0.2-fix.zip',
                         "addok-fr",
                         "addok-france"]
    }
)

