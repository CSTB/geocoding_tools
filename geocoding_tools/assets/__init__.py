import pandas as pd
from pkg_resources import resource_filename
from geocoding_tools.assets import dictionaries

reg_table = pd.read_csv(resource_filename('geocoding_tools', 'assets/reg2016.txt'),
                        encoding='cp1252', sep='\t')

dept_table = pd.read_csv(resource_filename('geocoding_tools', 'assets/depts2016.txt'),
                         encoding='cp1252', sep='\t')

com_insee = pd.read_csv(resource_filename('geocoding_tools', 'assets/communes2020-csv.zip'), dtype='str')

com_cp_poste = pd.read_csv(resource_filename('geocoding_tools', 'assets/2020_10_09_laposte_hexasmal.csv'), dtype='str', sep=';')

com_associe = com_insee.dropna(subset=['comparent']).copy()
com_parent = com_insee.loc[com_insee.com.isin(com_insee.comparent.unique())].copy()
com_parent['comparent'] = com_parent['com']
com_associe = pd.concat([com_associe,com_parent],axis=0)
com_associe = com_associe.drop_duplicates(subset=['com'])

old_com = com_associe.loc[com_associe.com!=com_associe.comparent]

geocode_cols_reserved_names = ['latitude',
                               'longitude',
                               'result_city',
                               'result_citycode',
                               'result_context',
                               'result_district',
                               'result_housenumber',
                               'result_id',
                               'result_label',
                               'result_name',
                               'result_oldcity',
                               'result_oldcitycode',
                               'result_postcode',
                               'result_score',
                               'result_street',
                               'result_type', 'is_geocoded']

result_cols_mapping_addok_search = {el.replace('result_',''):el for el in geocode_cols_reserved_names if 'result' in el}


def load_oldcity_ban(oldcity_ban_file_version):

    # fichier généré à partir du ndjson ban sur toutes les communes qui référence oldcity_code.
    oldcity_ban = pd.read_csv(resource_filename('geocoding_tools', f'assets/{oldcity_ban_file_version}.csv'),
                         encoding='utf-8', sep='\t',dtype='str')

    return oldcity_ban