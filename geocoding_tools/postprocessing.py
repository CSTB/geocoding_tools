from fuzzywuzzy import fuzz
import numpy as np
from geocoding_tools.addr_utils import clean_address_for_compare
from geocoding_tools.config import postprocessing_config
from geocoding_tools.config import debug_config
from geocoding_tools.assets import geocode_cols_reserved_names


def calc_partial_ratio(x):
    cp = x.result_postcode
    if cp not in x.address_sent:
        return None
    address_sent_no_com = clean_address_for_compare(x.address_sent.split(cp)[0])
    address_geocoded_no_com = clean_address_for_compare(x.address_geocoded.split(cp)[0])
    nb_words = len(address_sent_no_com.split())
    nb_words_sent = len(address_geocoded_no_com.split())
    if min(nb_words, nb_words_sent) < postprocessing_config['calc_partial_ratio']['MIN_WORD_NUMBER']:
        return None
    return fuzz.partial_ratio(address_sent_no_com, address_geocoded_no_com) / 100


def bad_geocoding_recovery_partial_ratio(df, address_sent_col='address_sent', address_geocoded_col="result_label"):
    df = df.copy()
    df['address_sent'] = df[address_sent_col]
    df['address_geocoded'] = df[address_geocoded_col]
    is_null = (df.address_sent.isnull()) | (df.address_geocoded.isnull()) | (df.result_postcode.isnull())
    df['recovery_score'] = np.nan
    df.loc[~is_null, 'recovery_score'] = df.loc[~is_null, :].apply(lambda x: calc_partial_ratio(x), axis=1)

    return df['recovery_score']


def remove_stacked_address_strict_best(df, unique_by=None,recovery_score_col='recovery_score'):
    """
    method to remove stacked address -> the best score is kept. This filter is to be applied to dataset that should have only one entry by address
    (ex. Registre National des Copropriétés,Données locales de l'énergie etc...). This filter only keep the best score in case of multiple address
    geocoded on the same result ban_id.

    Parameters
    ----------
    df : result geocoded dataframe from geocode_table.
    unique_by

    Returns
    -------

    """

    if unique_by is None:
        unique_by = []

    if recovery_score_col is not None:
        score_cols = ['result_score',recovery_score_col]
        df['best_score']=df[score_cols].max(axis=1)
    else:
        df['best_score']=df['result_score']

    required_columns = ['result_id','best_score']
    sorted_columns = [el for el in df if el in ['result_id','pro_ent_res', 'best_score']]

    if len(set(required_columns)-set(sorted_columns))>0:
        raise KeyError(f'missing columns {set(required_columns)-set(sorted_columns)}')

    df_sorted = df.sort_values(sorted_columns, ascending=False).copy()
    dup = df_sorted.duplicated(subset=unique_by + ['result_id'], keep='first')
    # on ne fait ce filtre que pour les housenumbers.
    dup = dup & (df_sorted.result_type == 'housenumber')
    if debug_config['debug_rejected']:
        df_sorted.loc[dup, geocode_cols_reserved_names] = np.nan
    df_sorted.loc[dup, 'is_geocoded'] = False
    df_sorted.loc[dup, 'geocoding_method'] = 'rejected : stacked address strict best score'
    df_sorted = df_sorted.drop('best_score',axis=1)
    return df_sorted


def remove_stacked_address_behind_best(df, unique_by=None, recovery_score_col ='recovery_score',res_tol=0.05):
    """
    method to remove stacked address -> best scores are kept. If multiple lines are affected to the same address we keep only the ones that are close to the best score.
    The hypothesis behind this filter is that from the same dataset different line with same result address should have been provided with the same
    initial quality. Therefore result score should be very closes and score that are behind are probably errors.
    Parameters
    ----------
    df : result geocoded dataframe from geocode_table.
    unique_by : list, default None
    res_tol : float,default : 0.05

    Returns
    -------

    """

    if unique_by is None:
        unique_by = []

    if recovery_score_col is not None:
        score_cols = ['result_score', recovery_score_col]
        df['best_score'] = df[score_cols].max(axis=1)
    else:
        df['best_score'] = df['result_score']

    required_columns = ['result_id', 'best_score']
    sorted_columns = [el for el in df if el in ['result_id', 'pro_ent_res', 'best_score']]

    if len(set(required_columns) - set(sorted_columns)) > 0:
        raise KeyError(f'missing columns {set(required_columns) - set(sorted_columns)}')

    multiline_best_score = df.groupby(unique_by + ['result_id']).best_score.max().to_frame('multiline_best_score')

    m = df.merge(multiline_best_score, how='left', on=unique_by + ['result_id'])

    bads = np.abs((m.multiline_best_score - m.best_score)) > res_tol
    # on ne fait ce filtre que pour les housenumbers.
    bads = bads & (df.result_type == 'housenumber')
    if debug_config['debug_rejected']:
        m.loc[bads, geocode_cols_reserved_names] = np.nan
    m.loc[bads, 'is_geocoded'] = False
    m.loc[bads, 'geocoding_method'] = 'rejected : stacked address around best score'
    m = m.drop(['multiline_best_score','best_score'],axis=1)

    return m